+++
title   = 'Oh the cliché!'
date    = '2015-01-29'
slug    = -16
+++

There are some conventions that have to be followed such as the ever present:

Hello World!

Almost certainly the first thing you learn in coding is to print this out, it is not a bad place to start a journey in written format.

My plan for these posts is to have a permanent home for most of the online stuff I do, from reading to gaming to data analysis to random thoughts I need to get out of my head.

Let’s hope I live up to my expectations!
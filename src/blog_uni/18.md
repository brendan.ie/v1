+++
title   = 'CS4178 - Week 11'
date    = 2022-11-20
slug    = 18
+++

[Rendered link][0]

Again it is really curious that we are still off by one week.  
If you were a student you would have failed the module due to submitting all your work one week after its due.

Related to above I am extremely displeased that you are going to foist two assignments at us at the same time because you "forgot" to post one for two weeks (as well as lectures or tutorials during that time).  
The very least I would expect from a professional is a written apology for teh time wasted.

Also for fecks sake, please __//**KEEP**//__ a consistent naming scheme, going from ``CS4178_Week9_2.pdf`` for Lecture 14 which was reasonably descriptive to ``CS4178_Lec15.pdf`` for lecture 15 back to ``CS4178_Week10_1.pdf`` for Lecture 16.  
The worst type of consistency is to be consistently inconsistent.

-------------------------------------------------------------------------------------------------------------------

For anyone reading this for UML stuff checkout [Mermaid.js][1] a tool to use markdown to generate uml diagrams deterministically, they also have an [editor][2].  
It really helped with doing the diagrams for CS4415.  
I am a major fan of markdown as its pure text but can be transformed into HTML pretty easily, just take a look at teh posts on my site.


[0]: https://brendan.ie/blog_uni/18
[1]: https://mermaid-js.github.io/mermaid/#/
[2]: https://mermaid-js.github.io/docs/mermaid-live-editor-beta/#/


+++
title   = 'CS4178 - Week 7'
date    = 2022-10-23
slug    = 10
+++

## Definitions

* Services are what you cna provide to a prospective buyer/customer.
* The language of the domain is important.
  
| Word   | Restaurant                                                 | Networking                                      |
|--------|------------------------------------------------------------|-------------------------------------------------|
| Host   | The owner of the restaurant                                | The machiene that services run on               |
| Server | A person who brings food/drinks to a table/client          | A program that runs on a host, often uses ports |
| Client | A person receiving teh service                             | A program receiving teh service                 |
| Port   | A drink that helps to extract information from the drinker | Special file to get information from the server |

* Did not expect to see CRUD here
* ERD's seem to keep following me wherever I go, slowly warming up to tehm 
  * mermaid.js ftw!

## Measurement
I think it's a tad too late to be doing measurements for an acceptance testing, that is anti-agile, should be doing it continuously.

Thankfully its relativly easy to measure and record QoS and SLA's as its normally something measureable, % uptime, XGbps, Y°C of a server room.  
QoE tends to be harder to measure as they can be a lot more subjective, best way to measure can often be tickets/complaints.

MTBF is actually easy enough to measure, as long as ye have adequate testing and foundation in statistics.  
Then again this could "fail" if it has a MTBF of 20,001 which is technically better.

First example for anti-virus is more about having reasonable requirements, sounds good on paper for management and above but in reality is virtually impossible.  
If its relaxed slightly to "all files scanned on additon to teh system" then it is actually the standard practice (as long as its not norton or mcafee, those two are garbage).  
Second example is a good example of reasonable/sane requirements, for anti-virus, but not for any other type of product.
+++
title   = "A shallow dive into teh joys of markdown" 
date    = 2023-09-15
slides  = true
+++


# Special Interest Night

----

#### A smol taster of Markdown

----

Markdown is a text formatting //language//

----

It turns

----

```markdown
# Hi there

Here __**have**__ a cat!  
![cat](https://cataas.com/cat/says/hello?width=100&height=100)
```

----

Into

----

# Hi there

Here __**have**__ a cat!  
![cat](https://cataas.com/cat/says/hello?width=100&height=100)

----

This means that if ye are ~~crazy~~ like me.

----

You can use source control (git) on yer documents

----

While still having tiny files,

----

that look danm good in the end.

----

Not only that,

----

Markdown is all around us!

----

From sites ye would expect

----

Like Gitlab and Github

----

To Lemmy and Reddit

----

To even our dearly beloved discord!

----

Dosent that make everything lovely?

----

And there was unity in teh galaxy?

----

There is unity, right folks?

----

...

----

Right???

----

...

----

# __//**FUCK**//__

----

So markdown was released in 2004 by John Gruber

----

Conceived as a fairly simplistic format it gained popularity.

----

However one issue was that what john created wasnt a specification.

----

It was a suggestion.

----

Moreover it only existed as a perl script.

----

No documentation.

----

There were some conflicting aspects.

----

``*`` was used to denote

----

* //Italics//
* **Emphasis**
* Lists (like this one)

----

The combination of these caused some syntactical issues.

----

Another quirk was to denote code you used four spaces at the start of teh line.

----

It just so happens that paragraphs in lists use the same idea.

----

This leads to inconsistent edge cases.

----

Github implemented their own //flavor// to suit their needs.

----

As ye would expect displaying code well is important

----

So its probally there that code blocks were invented.

----

They also wanted checklists

----

And everything else

----

Reddit came along with different wants

----

So they made their own flavor.

----

And so on and so on....

----

This is grand but ye end up having several conflicting standards

----

![standards](https://imgs.xkcd.com/comics/standards.png)

----

Totally not foreshadowing  
//dun dun dunnnnnnn//

----

So folks came together to create our lord and savior

----

# CommonMark

----

And there was peace in the world once more

----

For a while

----

CommonMark was just an amalgamation of all the existing standards

----

Lumped into one unholy mess

----

Instead of trying to fix things up

----

It decided that __//everything//__ was valid.

----

They also utterly fecked up handling of HTML

----

For no good reason.

----

(╯°□°）╯︵ ┻━┻

----

And then came Discord

----

Aside from using ~3 different flavors depending on platform

----

They also have different flavors based on context

----

They have all the good stuff

----

* Inline code
* Code blocks
* Emphasis
* Italics

----

And it now has headers and links

----

It used to be dire

----

But it has improved (thankfully)

----

But

----

It still has quirks

----

These two inline code snipped should have near identical results

* ``discord.gg/mkuKJkCuyM``  
* ``"discord.gg/mkuKJkCuyM"``  

----

However the second onw will have a "Join Server" Embed


![img.png](markdown/join_server.png)

----

Why?

----

Who feckin knows

----

So anyways

----

I made my own markdown standard (can find it on my blog)

----

This "PowerPoint" is made with Markdown

----

So teh entire thing is under 30KB (so far)

----

<button onClick="changeColour()">Click Me</button>

And I can do fun stuff like this

<script>
function changeColour() {
  document.getElementById("colourChanger").style.color = "red";
}
</script>
----
<button>Bye!</button>
<p id="colourChanger">
  And I can do fun stuff like this
</p>

----

For my final trick!

----

<button onClick="playStarter()">Play</button>

__**//warning//**__

__**//next part will flash, feel free to close tab whenever ye want//**__

----

<p id="video">
  <span id="video_top"></span>
  <br />
<br />
<br />
  <span id="video_bottom"></span>
</p>

<script>
let empty = 15;
let counter = 0;


const words = [
  "hi", "there", "how", "are", "you?", "I" , "hope", "you", "enjoyed", "my", "smol", "little", "powerpoint", "about", "markdown",
  "",
  "I", "said", "I", "would", "do", "901", "slides", "so", "I", "decided", "to", "make", "a", "'video'", "out", "of", "so", "many", "slides",
  "",
  "",
  "this", "message", "will", "repeat",
];

const words_cats = [
  "please", "send", "me", "all", "the", "cat", "pics", "you", "have"
];

function playStarter() {
  start = new Date().getSeconds();
  play();
  clock()
}

let clock_stop = false;
let elapsed = 0;
function clock(){
  if(clock_stop){
    return;
  }

  elapsed += 1;
  setTimeout(clock, 1000);
}

function play() {
  if(counter >= (820 * 10)){
    clock_stop = true;
    return;
  }

  if(empty === 45){
    document.getElementById('video_top').innerHTML = "";
    document.getElementById('video_bottom').innerHTML = words_cats[elapsed % words_cats.length];
    empty = 0;
  } else {
    document.getElementById('video_top').innerHTML = words[elapsed % words.length];
    document.getElementById('video_bottom').innerHTML = "";
  }

  empty += 1;
  counter +=1;

  // ~60 fps interlaced

  setTimeout(play, 17);
}

</script>

----

Thank you!


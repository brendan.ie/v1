# About This site.

### Background

So earlier this year (2020) I had gotten a hold of brendan.ie (this site).  
Email was easy to set up but what to put as the website?

I had toyed with the idea of using [IBM's Carbon Design System][IBM] to have a really slick homepage. I may yet do that in a future iteration.

I have also been toying with rust for a little while now, most notably my [trove_downloader][Trove] and more 
recently the [2020 Advent of Code][AoC2020] and wanted to do something with it again.  

Now I am well aware that there are many markdown converters and many many implementations of markdown, but I wanted to try my hand at it. [Another implementation wont hurt right?][There is an xkcd for everything].

[IBM]: https://www.carbondesignsystem.com/
[Trove]: https://gitlab.com/silver_rust/trove_downloader
[AoC2020]: https://gitlab.com/Brendan_Golden-aoc/2020
[There is an xkcd for everything]: https://xkcd.com/927/

### Converter

Well getting to this point too longer than planned.   
What I originally thought would be a nice //little// project before uni ended up spiraling out of control.

Ended up creating my own flavor of markdown for it.  
If you want to read my sinking into the rabbit-hole that is markdown I have it [here][I passed alice a while ago].

* [Gitlab][bfom gitlab]
* [Crates.io][bfom crates.io]

[bfom gitlab]: https://gitlab.com/silver_rust/bfom
[bfom crates.io]: https://crates.io/crates/bfom
[I passed alice a while ago]: https://gitlab.com/silver_rust/bfom/blob/HEAD/docs/Background.md


### This Site

This site itself is open source, you can find it on [Gitlab][site Gitlab].


[site Gitlab]: https://gitlab.com/brendan.ie/v1
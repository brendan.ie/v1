+++
title   = 'CS4178 - Week 6'
date    = 2022-10-16
slug    = 8
+++

Really interesting how our testing modules and this intersect, this week we were learning about non-functional requirements from both sides.  
At its core these are requirements that ye cannot definitely give a solid answer for.  
You can explicitly state and test that ``sum(1, 3)`` will equal 4, that is functional requirements.  
Non functional is everything around that:

* How maintainable is teh code.
  * Can new devs be brought up tos peed quickly
* What is teh documentation like.
  * is it beside the code or separately.
* How does it feel to use.
* How reliable is teh code/software.

The interesting thing is that these are all human problems that have human based solutions

* Code reviews, avoiding complexity for teh sake of complexity.
  * Mentors can really help, pair programming.
* Documentation can often get out of sync.
  * Jdoc solves this by having teh documentation as part of teh code.
* Testing, testing, testing.
* This covers every scope of the program, individual lines of code to teh system it is running on

Not sure why I focused on this, maybe it's because it's easier for devs to focus on functional requirements before non-functional requirements.  
Might just be at that stage myself where I like solid reliable items without fancy code-features that make it more complex for teh sake of complexity. (KISS)  
Not sure, will have to keep thinking about that.
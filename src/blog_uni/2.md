+++
title   = 'CS4178 - Week 1'
date    = 2022-09-25
slug    = 2
+++

# Week 1
No lecturer

# Week 2
New lecturer, Sahir Sharma  
I have some thoughts on teh CS4023 post.

Admin stuff for a good chunk of week 1.  
Need to figure out when the labs will be on.  
The fact that both modules are online is a blessing as it freesup my Thursday for travel.
Overview of module looks pretty decent, assessment seems it will be good as well.  
Module should pair well with CS4004, Testing.

Modelling = Breaking down reality into smaller subsections with reduced complexity, often involves making assumptions.

MDSE looks interesting but may have issues incountering things in reality that are not in teh model.
`
EBNF on the slides, didnt expect to see this here.

# Week 3
Good planning can lead to far less hassle down teh line.  
This involves figuring out:
**Who** and **how many** does **what**, **when** they do it, the **tools** they will use to crate the substeps  to be done by **deadline**.  
Part of the timing is ensuring people are not working on multiple projects which cna cause scheduling conflicts.


However too much planning can be toxic for everyone involved and can be a sign of bad management.

## Waterfall
Everythign is planned out at the start and followed through step by step.  
At the end if there are any issues the process is restarted from the top.

This process may work very well in manufacturing however it begins to fail with faced with anything either complex or creative.

## Agile
Agile is one framework which seeks to tackle this balance.
It was created as a response to Waterfall and other process methodologies which favored management and the process over
the delevopers and actually creating teh product.

The core of it is below.

> Individuals and interactions over processes and tools
> Working software over comprehensive documentation
> Customer collaboration over contract negotiation
> Responding to change over following a plan

While its concepts are still useful today it is rare to see it in practice since it has been co-opted by management to suit their needs.  
There have been far far too many stories of delevopers spending more time in meetings/standups than actually doing their work.  
You can still find places where it is done as intended but thoise are far and few inbetween now.

# About
This page exists because the default formatting for the UL co-op CV has the text as one giant blob.  
So here is how the student input section should look like.


# Teamwork
Due to having prior experience before arriving at college I  
took it upon myself to act as a mentor to my classmates.

This has involved streaming lessons, code review and directing  
them towards tools, reference materials and processes.    
Occasionally this means that if I spot them heading for a pitfall it  
can often be better to allow them to make the mistake and use it as a  
learning/teaching opportunity than to correct them in advance.  

I was formerly involved in both the Diving and Gymnastics clubs.    
Now I am a member of the Computer and Philosophy societies.  

I was also a member of the team representing UL in the 2022 Robocode    
competition

# Communication Skills
Communication has been essential for virtually all the projects  
I have worked on as they required interacting with the communities  
to figure out what users wanted.

Much of this took place on platforms like IRC, Slack and Discord.  

Communication is also evident in how issues were raised on said  
projects and subsequently resolved.  

# Problem Solving & Analytics
During the last semester I got alerted with an email that my main  
server had been quarantined with one possible outcome that it  
would be wiped.  
Due to treating the server like a pet over the years I did not  
know the config to create a new instance of it.  

The problem I identified was a way to securely record the  
configuration of the system as well as with a need of adequate  
backups.  

Upon research, I discovered that NixOS was near perfect for my  
needs as it gave reproducible builds of the OS and all installed  
programmes.  
Using a parallel VM I was able to duplicate the config and convert  
all of my applications to using Nix Flakes.  

When it was complete I had a self documentation config for my server  
with backups enabled.  
As a side benefit it also simplified my CI/CD deploy process leaving  
less surface area for intrusion.  
Not only that I now have a complete history of every change,  
timestamped and signed.  

# Using Initiative
As college students we get multiple assignments for each module.    
Each assignment has its own deadline, together it is easy to lose  
track of what is due when.  

The goal was to create a bot that could list out the assignments  
the user had due, based on the modules they were registered for.  
A secondary goal was to provide a platform that classmates could  
create and integrate their own commands.  

Using Typescript and Gitlab's CI/CD I created the skeleton of the  
bot to address these needs.    
Documentation for how to add modules was also drawn up.  

Due to the visibility of the assignments in a centralised location  
students started completing assignments earlier with fewer missed  
assignments.  

During the summer months several classmates have created their     
own commands.  

# Projects/Portfolio/Volunteering
Gitlab:	<<https://gitlab.com/Brendan_Golden>>

Notable projects:

BnS_API - Node.js/MongoDB/CICD	        
My longest running project. It contains many of my early failures,  
and the refactors done to correct them.  

DataWars2_API - Node.js/MongoDB/TS/CICD  
This has been where I have done a lot of experimentation. I also  
provide a live service for countless players of GuildWars2  

Trove_Downloader - Rust/CICD  
This was built to help learn Rust along with automating an issue  
away.  

BFoM - Rust/CICD/Markdown  
I created my own flavor of Markdown, a sane flavor.  

Brendan.ie - BFoM/HTML/CICD  
This is my own personal site.  
I wanted something that did not rely on anything else like bootstrap  
or react.  
So everything is by my own hand.  

Main server - Nix/NixOS/CICD  
As outlined above my main server is now running NixOS.  
This has the advantage of reproducible builds.  


In Secondary I was on several teams including:  
* NASA Ames Space Settlement Design Competition
* F1 In schools


# Additional Information
* Gitlab: <<https://gitlab.com/Brendan_Golden>>
* Linkedin: <<https://www.linkedin.com/in/brendan-s-golden>>
* Formatted CV: <<https://brendan.ie/cv/ul>>

+++
title       = 'Mass Effect 1'
date        = '2021-12-18'
slug        = "7/1"
skip_index  = true
+++

According to steam teh last time I played this was back in 2013, I had bought it a month or so before that.  
However, because of the timeframe I know I had a high seas version before that.

It seems I had slight rose tinted glasses on for this game.  
There were a few things I was right about but others I had completely forgotten or intentially forgotten.  
For the most part the game was good but there were a few areas that let it down.

## Bad
### CTRL+C CTRL+V
I swear for most of the offworlds there were 4 types of buildings that all had the same layout:

* Ships
* Warehouses
    * The two "unique" ones were in the DLC
* Underground labs
* Mine

Same layout in every one, copy and pasted.  
Thankfully though this is really the only part that drags down teh game.

## Good
I was going to use headings such as //story// or //characters// and maybe //music//, however I figure it would be best to talk about it in terms of locations.
Because despite what I said earlier about bland offworld locations its a few key locations that elevate and tie everything together.

### Normandy
This is your home and refuge for the game, tense music can often give way to lighter refrains.  
You talk to your companions about their hopes, dreams and woes.  
From here you have the freedom of teh entre universe.

### Citadel
This really is the main hub, its vibrant and open, shining and chrome.  
A multitude of people and regions, both good and ill.

### Feros
A land in the clouds and a conflict between far off corporation adn local interests.  
Aside from the hardest charm check in teh game.

### Noveria
A frozen land of corporate backstabbing, along with playing god.  
These frozen levels were some of the most memorable in the entire game, along with teh variety of foes and characters in both the corpo and research areas.  
From my memory of ME3 the choice here is really important.

### Virmire
By the time I had gotten here I had already gotten the achievement for most of the game complete.  
As a completions it was natural to get the best ending here, completing all optional objectives as well as taking care of all the prisoners.

### Citadel - Final
Danmmmmm this was so good, a simple enough map but the combination of the music and >!Sovereign!< looming over you gives such a sense of being merely an ant.  
As a climax to this game it is fitting.


## Conclusion
If the game released now it would be slated for being bland, but for its time it did release it was good and exciting.  
It is clear in hindsight that many games took notes from ME1, but even with that it holds up well enough, doubly so with the graphics overhaul.

## Stats:
* Completed (100%)
    * 272 saves
    * 33.69 hours
+++
title       = 'Far Cry'
date        = '2021-09-19'
slug        = "5/3"
skip_index  = true
+++

<!--I really have to finish this before FC6 comes out...-->

Far Cry is a series that is pure chill out for me (along with Just Cause). 
At the same time it also (moreso in more recent times) questioned my actions.  


### Far Cry 3
Like my folks my first entry into FC was FC3.  
At the time I was on a secondhand dell system, an Athlon 64x2 and something like 512mb of vram, tiny by modern standards but the game ran smoothly for me and was breathtaking.  
A huge expansive world to explore (and get killed by tigers).  
I honestly cannot remember too much about it now because it was so long ago.

As a sidenote I never played Blood Dragon, please dont lynch me.


### Far Cry 4

A benefit of being a (sometimes) patient gamer is you learn stuff about the game before you play.  
In my case this was the secret ending at the beginning where if you actually listen to the antagonist and follow their command you get a special scene.

Sadly though I just couldn't get into the gameplay, something about it just didn't mesh with me.


### Far Cry 5
I played 5 not too long ago so its still fresh enough in my mind.  
Beware there are open spoilers, tis difficult to discuss it otherwise.

5 released at a weird time, Donalt Trump had been in office for two years and America was (at least in Irish media) a parody of itself.  
Between civil unrest, police being considered //unreliable// and evangelical fanatics it wasn't looking too good.

Along comes FC5, a game that wears all of that on its sleeve.  
We have a religious fanatic that is leading (what seems to be) a death cult.  
Regular folks are taking up arms.  
And we, the Deputy who has only been on the job a few weeks is the only instance of law and order.


#### Choices ~~and consequences~~
Dotted throughout the game are choices, the first within moments of starting the game.  
These mostly take the form of an icon appearing on screen telling you to press a button.  
The thing is you don't have to press that button and you are //rewarded// for not doing so.

This is a tiny detail I feckin love.

#### Main Ending
Welp, turns out that the //fanatic// was right.  
That really asks the player about their actions.

Is the Player Character right just because the Player is the one at the controls?  
Is the Player always right?  
Why is the Player always right?  
Was the Player manipulated?  
What are the implications of this?

These questions are not unique to this game, years ago I heard [Through the Night][Malukah] sung by Malukah that got me thinking along these lines.


[Malukah]: https://www.youtube.com/watch?v=XA-dYfIW60E

##### How did he capture your allies?
Who said he captured them, for all we know they were already part of his congregation, sleeper agents essentially.

#### Secret Ending
Like FC4 5 has its own secret ending at the beginning, if you follow the commands of the antagonist you get an interesting ending.  
On its own its pretty neat.

However once you take into account the other endings, specifically that you are a sleeper agent for the cult.  
This means that this ending is a nice nod to the future of the game that takes hours to pay off.

### Far Cry New Dawn
This was an interesting game, it dealt with the consequences of what happened in FC5.  
You had the folks you would have expected to be racist hillbillies creating sustainable homes and generally being a vision for the future.  
The cult that predicted all of this returned to the land and became a peaceful, enclosed settlement.  
Stumbling across one of the old bug out bunkers was always interesting.

And then you had the folks that couldn't adapt, waging violence on all who had turned over a new leaf.

Like FC5 it has its own mini choices with buttons on screen.

I really enjoyed the levelling system where you had to complete achievements with the weapons.  
This worked quite well as many of them were unconventional weapons and got me to change up my playstyle, otherwise I am liable to turn into a stealth archer/sniper.

The one area I didn't enjoy was outposts, I had expected that when you vacated one the raiders would return and literally build it up, add towers, walls and wahtnot to change the layout.  
What we got was more/higher guards and the alarm locations increasing/moving.  
I ended up downloading a trainer to give myself all the ethanol I wanted after I claimed all the outposts the first time.

### Far Cry 6
//Checks date//  
I am eager for the release of FC6.  
I hope that it will have a secret ending like the ones outlined above.  
I hope that it asks the players to deal with consequences of a revolution (who knows, life could be terrible if your side wins).  
I hope that i will lose myself in this new world.

Unlike previous games I have pre-ordered it and at this moment it is pre-loading.
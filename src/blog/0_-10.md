+++
title   = 'Hello, I am Silver and this is my journey.'
date    = '2015-04-10'
slug    = -10
+++

It is not every day that you learn that your brain is wired differently than the general population.

This is completely a self post, just me talking about my thoughts and experiences from the past two years. If that type of thing is not your taste turn around, because I am not that good about talking with myself as the main topic and it is going to ramble.

Almost a year ago I requested and received a leave of absence from my university. There were three main reasons:

I had my appendix removed – it was almost about to burst when they operated, the recovery time ate into my co-op period.  
Here in Ireland the cost of College rose dramatically when the government withdrew college funding in the recession, what should have been enough for four years only was enough for two. I had to get a job to gather funds.  
Thirdly, to figure out what was up with myself, why my wrist would commit mutiny after 1.5 A4 pages, why did the lack of a solid structure to the day effect me so much.  

Well it has been a long journey, it took five months to get an appointment with a Development Psychologist. There I learnt:

* I am dyspraxic – no wonder I am not fond of team sports.
* I am dysgraphic – part of dyspraxia it relates to poor handwriting.
* While she was not qualified to diagnose it she did recommended checking out a Clinical Psychologist about Aspergers.

While I was somewhat expecting Dyspraxia (though not to that severity – I am in the 4-10 percentile region) the aspergers came as a surprise. Here I was at the age of 20 thinking I knew all about myself, turns out that knowing yourself takes a lifetime. On top of that part of the testing was an IQ test, it both stroked my ego as well as throw me off balance to learn that I qualified to be a member of MENSA.

Shortly after that report came back my laptop gave up on me, the motherboard was gone and repairing that would have been prohibitively expensive. I do a lot of communication via the Internet, IRC, teamspeak, games reddit and other forums. I tried to compensate with my phone, a modest Samsung Galaxy S but despite it being amazing it still couldn’t keep up and I drifted away from the people I was regularly in contact with. My part time job if data analysis was left as it was, I had no way to securely access the data – I do not trust library computers for that task.

So I had three months where I was not working and where I was out of contact with most of the communities I was part of. Needless to say that was my darkest time, it seemed that the people who were supposed to help me were against me, whereas in reality they were trying to aid me all along.

It was only when I attended the anniversary mass of one of my friends in early January (who had committed suicide the year before) that I started to come back to normality, also in the same week I received an old desktop – Dell Dimension E521 – I arranged to meet the mother. In the times I met her afterwards I learnt a lot more about the friend I had met in college. When she was describing him pre college it was almost as if she was describing me. It utterly shocked me how close it had come to there being two anniversaries so close together.

It was after that the darkness began to lift. I started working for the same company as I had before, I added a few upgrades to the computer, with more planned. I began to plan and dream about the future again.

Midway in February I got the Clinical Psychologist and received the report back. Under DSM IV I was diagnosed as aspergers (in DSM V it is now classified as ASD Autism Spectrum Disorder, she chose IV because more people would recognise aspergers.

Because of this the Co-op (placement) office in my university has been able to arrange an interview with a very good company that I would not have qualified for before, even better this company actively seeks people with different viewpoints – specifically disabilities. This because they are involved in research an innovation.

What’s more when I head back to college I will be getting supports, mostly in the realm of getting copies of the lectures as well as tutoring on organisation. It may have been a huge amount of work sorting everything out, but once that is done there are excellent services to be availed of.

So that brings me up to the present, mostly. I started this WebLog with the encouragement of the mother mentioned earlier, to use it to work on getting my feelings out and to stop bottling them up while remaining fairly anonymous for the time being. She believes that if her son had not kept things to himself he would be still alive. This post has been a draft in various forms for a few months, first on my phone, then email and finally here. I have used it as a tool to help me to come to terms with who I am. It has helped me realise that although I now have several labels they don’t change me but rather reveal more dimensions about myself. It has also helped me come to terms with the fact that having labels like this – being officially recognised as being this way – has both downsides and advantages, the positives do outweigh the negatives though. It has taken me quite a while to realize that I do not have aspergers but rather I am aspergers, a change in perception that symbolises my journey thus far.

So thanks for listening through my ramblings, if you wish to listen to more of them in the future I finally figured out how to set up the “Subscribe!” option, you will find it to the right of this.

Farewell and have a great day!
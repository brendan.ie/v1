+++
title       = 'AC: The Mythological Trilogy + Fenyx'
date        = '2021-09-19'
slug        = "5/2"
skip_index  = true
+++

This was a fresh start for teh series, and by the looks of it, it was quite successful.

As regards to open world one of the key changes was what I call ``Climb Everywhere`` where if you see something you can climb it.  
This is a departure from the "Classic" games where you could only climb on certain features with "paths" leading up to eagle points.  
As well as that teh map itself is far less vertical favoring large expanses it feels somewhat lazy.  
Just point yer character at a mountain and keep the ``W`` key pressed and within a few min you are at the top, you can even do it blindfolded.

### Odyssey
Ship sailing was back on the menu with this game which did make travelling long distances far more enjoyable.  
Coupled with the effortless climbing there was also the ability to negate fall damage, these combined made it a joy to climb mountains, although it also devalued leaps of faith.

What made the game really stand out is how good a character Kassandra was

### Valhalla
Games are supposed to improve over time, not degrade.  
While the game does look good and building up Ravensholm is neat (a nice callback to AC III) the story itself is lackluster.  
So lackluster that I have done a 100% map completion of the core game but couldn't continue to play.  

The game still kept the ``Climb Everywhere`` motto which was nice for Origins, excuseable for odyessy (Kassandra is close enough toa  demigod)

Another area the game faceplanted was the Order of Ancients.  
You could guess who the (main) undiscovered members were by their silhouette long before they were officially revealed.  
Tot only that but you could see them pretty clearly if you turned up teh brightness.  
In Origins you would only figure out if they were male of female, in Odyssey you could somewhat identify some of them based on clothing, but they all wore masks which obscured them far more.
Then there were so many "normal" villagers who were members of the order, they wre just chores.  
The Irish expansion also had this exact same flaw which was annoying af.

The highlight for me was a mission part way through that stripped you of your armor and weapons, depowering you.  
This entire map felt close enough to the spin off games in teh Far Cry series, (Blood Dragon/New Dawn) and was absolutely stellar.

### Immortals Fenyx Rising
Interestingly enough I think mechanically //this// is the best (modern) AC game, despite not being an AC game.  
Stealth is reasonably good with foes not detecting you immediately giving you a chance to hide.  
Gameplay wise climbing is now limited by your stamina which puts you in teh mindset of planing your climbs and when you do reach the top you do get the sense of achievement.  
It also makes the maps feel far bigger than they truly are.

Gliding is also a really nice way to traverse the map

Aside from the normal enough map design the characters are pretty danm amazing.  
Even the mundane action of opening a chest has multiple possible animations which combined make it far more enjoyable to watch than a generic action 100's of times.
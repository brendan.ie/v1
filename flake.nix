{
  description = "Brendan's Personal site";
  
  inputs = {
    utils.url = "github:numtide/flake-utils";

    # nix flake lock --update-input bfom
    bfom.url = "gitlab:silver_rust/bfom";
  };

  outputs = { self, nixpkgs, utils, bfom }: utils.lib.eachDefaultSystem (system: 
    let
      pkgs = nixpkgs.legacyPackages."${system}";
    in rec {
 
      # `nix build`
      defaultPackage = pkgs.stdenv.mkDerivation {
        name = "brendan.ie";
        src = self;
        buildPhase = "${bfom.defaultPackage."${system}"}/bin/cargo-bfom";
        installPhase = "mkdir -p $out; cp -R build/* $out";
      };

    });
}
+++
title   = "Doubts and fears are wonderful, aren't they?"
date    = '2021-09-18'
slug    = 4
+++

#### ~~2021-09-12~~ ~~2021-09-09~~ ~~2021-09-03~~ ~~2021-08-30~~


I am fecken terrified of heading back to university.  
You can probably see that the dates above, dates I had planned to release this public.

To understand why a bit of background is required.  
When I was in primary school I picked up a coursebook and somehow decided that doing Aeronautical Engineering was the best thing I could do with my life.  
For teh most part this was a good thing, it gave me something to focus on through primary and secondary.  
However, blinkers aren't always a good idea, they can allow you to be blindsided pretty easily.

In uni I began to have issues, issues that I had for years but were glossed over in second level due to being able to preform well there.  
I didn't know how to ask for help, or for that matter when to ask for help.  
I had issues with not being able to write and listen at the same time, teachers will wait for ye, lecturers don't have that luxury.  
And the final piece for me was a member of the CAD group I was in committing suicide, at teh time I thought I was fine but as it turns out I had no idea how to deal with grief/death/emotions.

This all lead to me burning out and breaking down, a process I don't wish on anyone.  
Thankfully though I learnt (in most cases) how to deal with them.  
- I've learnt to ask for help.  
- I've learnt to say no.  
- I've learnt how to actually speak my mind.  
- I've learnt (somewhat) how to network.  
- I've learnt how to learn.  

So now I am heading back with all those fears and failures from before, I know I'll be adding a few more from this time round. 
One of the more interesting things is that the uni gave me my old ID number again, so it kinda feels like a nod/acknowledgement of my previous time there.  
Anyways here's to the next four years of success, failures and subsequent successes.
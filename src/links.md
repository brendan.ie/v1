# Links
Here you will find several links to accounts of mine.


* [Homepage][home]
  * [Coffee][home_coffee]
  * [Random Rickroll][home_rick]
  * [ᚁᚏᚓᚅᚇᚐᚅ][home_ogham]
* Git Repos
  * [Gitlab][gitlab]
  * [Github][github]
  * [Skynet][forgejo]
* [LinkedIn][linkedin]


[home]: https://brendan.ie
[home_coffee]: https://brendan.coffee
[home_rick]: https://hello.brendan.ie
[home_ogham]: https://ᚁᚏᚓᚅᚇᚐᚅ.com
[gitlab]: https://gitlab.com/Brendan_Golden
[github]: https://github.com/Silver-Golden
[forgejo]: https://forgejo.skynet.ie/silver?tab=activity
[linkedin]: https://www.linkedin.com/in/brendan-s-golden/

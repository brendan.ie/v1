+++
title   = 'CS4178 - Week 4'
date    = 2022-10-02
slug    = 4
+++

This week is really quiet as regards topics.  
Some ideas seem like they exist just for teh sake of existing.  
A large issue is teh lack of names for types of documents, can make it really hard to find information if ye are searching for it.

What I found useful this week was ``Stakeholders/Goals table``, its clear, concise and actually useful.  
``Swimline Chart/Table`` was also interesting, may be good for control flow through teh happy path.
``Use Case`` This one is a bloody nightmare, the name is vague enough that google showers everything at ye and 99% of all material about tehm was about charts, nothing like the template.  
Incidentally it showed how bad documentation can lead to unintended side effects.

---------------------------------------------------------------------------------------------------------------------------------------------------

Oh Sahir please fix yer naming scheme when ye get full access to Sulis (Ye should be able to do it already regardless).  
``CS4178_Week1.pdf`` vs ``CS4178_Week1-1.pdf``  vs ``CS4178_Week4_lec1.pdf`` vs ``CS4178_Week4_lec1-1.pdf``
Try ``CS4178/Lectures/Week01.pdf`` and ``CS4178/Labs/Week04.pdf``, good organisation leads to better results for everyone.
# Hello, I'm Brendan

I am a <span id="age">27</span> year old full stack web developer, currently studying Computer Systems at University of Limerick, Ireland.

I was previously studying Aeronautical Engineering, but I was not a good fit for that line of work and decided to do a bit of soul searching

I was lucky enough to find mentors like [David Reess][David] and [Greaka] who nudged me in the right direction.  
This then lead to starting with node.js, mongodb and git.  
From there I explored many different languages and tools, a selection of which can be found on the project table below.

However dispite all I had learnt I felt lacking, so in September 2021 I returned to University to study computer science.

# My projects

The majority of my work is on Gitlab as open source projects, oft to solve an issue I have.  
Below are some projects that have been milestones in my journey thus far.  
See my Gitlab profile above for everything else.

| Project                           | Start   | Tools/Tech                              | Notes                                             |
|-----------------------------------|---------|-----------------------------------------|---------------------------------------------------|
| [UL Timetable to ical][timetable] | 2022    | Rust, NixOS                             | [A dev can make their own tools.][timetable-site] |
| [Nixos on my webserver][Mixos]    | 2022    | NixOS                                   | [See my post about it][post-nixos]                |
| [Better M7 Express][betterm7]     | 2022    | Rust, NixOS                             | [Can I build a better app? Yes][betterm7-site]    |
| [Classbot]                        | 2021    | TS, Node.js, Sqlite3, NixOS, CI/CD      | A training tool for my classmates.                |
| [hello.brendan.ie][hello]         | 2022    | Rust, NixOS                             | [Will it Rickroll?][hello-site]                   |
| [This Site][this-Site]            | 2021    | Rust, Markdown, NixOS, CI/CD            | [Want some Coffee?][this-Site-coffee]             |
| [Markdown Converter]              | 2021    | Rust, Markdown                          | [This was a "mistake"][post-bfom]                 |
| [Fo76 Tools][Fo76.ie]             | 2020    | TS, Node.js, PostgreSQL, Express, NixOS | Experimentaions with docker                       |
| [Trove Downloader][Trove]         | 2019    | Rust                                    | First rust project                                |
| Homelab                           | 2018    | Linux, Networking, Docker               |                                                   |
| [DataWars2 API][DataWars2.ie]     | 2018    | TS, Node.js, MongoDB, Restify, NixOS    |                                                   |
| [Unofficial BnS API][BnS]         | 2017    | TS, Node.js, MongoDB, Restify, NixOS    | First project                                     |

# History

| Location                | Start | End  | Position           | Notes                         |
|-------------------------|-------|------|--------------------|-------------------------------|
| University of Limerick  | 2022  | 2022 | Student Researcher | First year UL has run UPSTarT |
| University of Limerick  | 2021  |      | Student            | Computer Science              |
| Penny Medical           | 2016  | 2020 | Technician         |                               |
| Abbott Vascular         | 2015  | 2016 | Intern             |                               |
| University of Limerick  | 2012  | 2016 | Student            | Aeronautical Engineering      |

<!-- Overall format shamelessly copied from David Reess's own page -->

<script>
/* vanity, dont want to have to manually update it after this */

/* calculate the age */
let ageDifMs = Date.now() - new Date("1994-02-01");
let ageDate = new Date(ageDifMs);
let age = Math.abs(ageDate.getUTCFullYear() - 1970);

/* set the age */
document.getElementById('age').textContent = age.toString();
</script>

[Gitlab]: https://gitlab.com/Brendan_Golden
[David]: https://david-reess.de 
[Greaka]: https://github.com/greaka
[Fo76.ie]: https://gitlab.com/Silvers_General_Stuff/fo76_trading/compose
[Trove]: https://gitlab.com/silver_rust/trove_downloader
[DataWars2.ie]: https://gitlab.com/Silvers_Gw2/Market_Data_Processer
[BnS]: https://gitlab.com/Silver_BnS
[Markdown Converter]: https://gitlab.com/silver_rust/bfom
[this-Site]: /site
[Mixos]: https://gitlab.com/nix17/nixos-config
[Classbot]: https://gitlab.com/c2842/misc/classbot
[post-nixos]: ./blog/11
[post-bfom]: ./blog/1
[timetable]: https://gitlab.com/c2842/misc/ul_timetable_ical
[timetable-site]: ./timetable
[betterm7]: https://gitlab.com/silver_rust/better-300
[betterm7-site]: ./bus
[hello]: https://gitlab.com/silver_rust/redirector
[hello-site]: https://hello.brendan.ie
[this-Site-coffee]: https://brendan.coffee
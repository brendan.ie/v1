+++
title   = 'The Perils of being a sheep.'
date    = '2015-04-14'
slug    = -9
+++

There are times when following the advice of an “expert” can be more for their benefit than yours. Here is a piece of market manipulation that irked me a few months ago.

```
Unfortunately I am new enough to using a weblog, and completely forgot my manners, I should have given the curtsey of contacting him first for at least his comments on the matter, I did not and ended up accusing him of manipulation. We made contact (comments below) and I finally got his side. It is a lot more innocent than I initially thought.
But still the basics of this still apply, essentially always do your own research and homework before just jumping into an idea that someone tell you about! Only invest when you agree with the reasoning and research.
Happy Trading!
```

Hey all!

I have been experimenting with the TradePost almost as soon as I started the game, it is an exciting place, you can get almost everything there, consequently this means it can often be overwhelming.

But fear not!  
There are numerous blogs where people can guide you through various aspects of making a profit off the TradePost. When they are starting out they would have to be highly truthful, but as time goes on and they gain a following they can begin to actually manipulate the market through the sheer number of people following their ideas.

Here is one such manipulation from a normally reputable blog which has several guest writers, all of which do have some really good ideas.

The Ugly Gift  
On November 6th 2014 Draco [Posted] about buying the Ugly Clothes (Socks, Hats, Sweaters) so that when the Wintersday seasonal event arrived it would be possible to trade these in in exchange for gifts which would then be sold.

Lets get this straight, he was suggesting that people buy up items where there was virtually no supply and where there would be a near infinite supply arriving a few weeks later when the people were supposed to exchange their ugly stuff for nice gifts. That does not sound like especially good advice. I wonder if there was an ulterior motive behind it…

Now the best way to figure out stuff is using data, thankfully with the Guild Wars 2 API we have sites like gw2spidy.com which keep accurate records of every item on the TradePost. So lets delve into the data in question (don’t worry I will make it kind on the brain). Below find the graph for Ugly Socks for the period October 14th to December 25th. [Item Page]

Now, as per Draco’s article the buy instantly price is 33c. It is quite clear that the post had quite a large impact on the market, within 10 days the supply has halved (from 1.4 Million to 0.7 Million) and the price has risen by 52% to 50c. However in the same period buy orders remained constant. This implies that the people who bought socks were impulse buyers as it is the type of buying where you rely on other people to make a call and you follow it.

That is looking after the post, but what happened before? From the 20th October to the 27th 400,000 ugly socks were bought pushing the price from 17c up to 33c. Between the 27th and the 6th the number of listings rose back to its previous levels. This is an item where the only way to obtain it at the time was through a Wintersday gift, so expectedly new supply would be almost zero.

Its almost as if someone knew that there would be a huge rush to buy Ugly Socks. My guess is that they did know. Attached to guildwars2tradingpost.com there is a forum, you pay (real money) to get in but there is an unspoken guarantee that there would be plenty of profit from the TradePost. About 1000g worth of Ugly socks was bought two weeks before the post, and subsequently sold for a minimum of 1440g (min profit of 224g) (not counting the other 550,000 socks that sold nor the sweaters or the hats).

The plan was that when WintersDay went live that people exchange their uglys for Gifts, lets have a look at how that turned out.

As you can see Generous, Giant and Small Gifts fell while Large and Medium rose, it may have made some people profit but it would have been more profitable to have bought the uglies at the reduced prices during the event (13c) rather than the prices recommended before it (33c).

The summary of it is:

* If an expert is offering advice always verify it they have an ulterior motive behind it.
* Trust your instinct, if you don’t feel right about it, it is probably not a good idea.
* When dealing with the TradePost buy the mats while the event is in progress, the huge supply makes the price plummet.
* For the non-statistics orientated people don’t forget to enjoy the game Tyria is a beautiful land to explore, spending all day in front of Gnashblades Tradepost is often not the most exciting thing.


Best of luck!

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

**Recovered comments** (manually typed word for word, my own comments are missing)


```
David 2015-04-14: 
Yea, i have a reply to this.
The Ugly's were being suggested by ME DRACO sure to last years conversion rates. If you would of done any research like any other TRADER, you would of seen how last year was handled. This year, not sure if they were going to do the same, but hoping they would, I suggested following my lead of buying up said Ugly's hoping to profit off the conversion if held true to the prior year.
And you would also know that the reasoning behind this didn't work because ANET decided to use different items then they did the previous year.
So for your 'Theory' of sheep herding is inaccurate from the point your trying to make.
Now let's look at something else:
You stated "Lets get this straight, he was suggesting that people buy up items where there was virtually no supply and where there would be a near infinite supply arriving a few weeks later when the people were supposed to exchange their ugly stuff for nice gifts. That does not sound like especially good advice. I wonder if there was an ulterior motive behind it…"
Yes there was an ulterior motive behind it. Anybody with any common sense would know that if something is low in supply and there is demand for it, guess what? It goes up. If there is a very strong demand it goes up fast! Why would i recommend this then? Here is my ulterior  motive. To Make a profit while buying cheep and after obtaining the position of the Ugly's i was happy with, let others on the opportunity to make a profit. What you ASSUMED thinking that making a few coins over the potential that was originally there via the conversion profit was manipulation!In actuality the conversion profit would have way overshadowed the smaller profit of the Ugly's moving. Another note: Before all holiday events, holiday event items from the past typically move up in value in anticipation and speculation that they may be used and thus causing those items to move up.
I'm am sure you didn't bother to do any research other holiday items to use as a reference. Either your not making gold in the game and pissed about it, or you like many other that listened to me and didn't make money on this idea and still just pissed.
Before anyone just jumps into an idea, they should always do their own research and homework and not follow the others.
Yes, Bulls and bears make money in the market, Sheep get slaughtered!
You ever want to talk about how i come up with ideas (good ones and bad ones) hit me up in game. Be more than happy to chat.
```

```
David 2015-04-14: 
First let me be absolutely clear, it's not my site, nor do i claim any partnership to it. I started using the site for trading ideas. I did start to used the payed forums as well. I also became an administrator for the forum as well. If anyone that has access to it can attest, i put a lot of ideas on it. Not just speculative ones. many are ways to make gold (not a billion overnight) on a slow but steady consistency. Looking forward to chatting with you in game.
```



[Posted]: https://web.archive.org/web/20150418081605/http://www.guildwars2tradingpost.com:80/2014/11/ugly-profits.html
[Item Page]: https://www.gw2spidy.com/item/38448
+++
title   = 'And Hello to you too Reddit!'
date    = '2015-03-29'
slug    = -12
+++

There are sometimes when a domains reputation and history can work against you

While I may love the uniqueness of my .tk domain it does come with a price. Because it is a free domain it was often used in the past for phishing and spam sites, and that shadow still persists to today. [Wikipedia] has a good summary.

The way this affected me is that it appears that .tk can trigger the spam filter for some subreddits, if this happens you, the poster can still see it but nobody else can. This is not the best thing for your self confidence as it seems like you are being completely ignored.

Anyways I got that sorted now, I am now the owner of http://www.silveress.com (1) , if you use this address it will forward you on to what I was using all along. anything like silveress.com/EpicRandomStuff will go to silveress.tk/EpicRandomStuff. This should put me in the clear for any filters that don’t like my personal choice in domain.


-------------------------------------------------------------------------------------------

(1) Not any more, lost the domain in the interim.

[Wikipedia]: https://en.m.wikipedia.org/wiki/.tk#Abuse
+++
title   = 'CS4178 - ~~Week 8~~ Week 9'
date    = 2022-11-06
slug    = 14
+++

[Rendered link][0]

No new material this week, either lectures or slides.  
Spent time reviewing notes and other folks blogs.

CS4023/CS4013 and CS4004 have priority this week due to projects and assignments.

-------------------------------------------------------------------------------------------------------------------

We did get Week 8 lectures uploaded in Week 9, I dont think DLS is teh cause of that......

**EDIT**: Reading through them, I am curious if our lecturer has done the analysis of prioritising his own education over the job he was hired to do (educate us).  
Because clearly he opted to focus on himself.

[0]: https://brendan.ie/blog_uni/14


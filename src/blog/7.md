+++
title   = 'Mass Effect'
date    = '2021-12-18'
slug    = 7
+++

So the Mass Effect Legendary Edition came out a fair while now, thought I hadn't a chance to play it until now.  
So here are my thoughts on the series I play through.

I last played the series back in 2013, I was a very different person then, different mindset, different ideals, different experiences.

--------------------------------------------------------------------------------------------------------------------------------------

## Individual reviews

For ease of containing spoilers and to avoid each part seeing too long I have broken up my thoughts into one page per game, returning ehre for the final overall conclusion.

| Sub-Post | Date Started | Date Finished | Saves | Time (100%)   |
|----------|--------------|---------------|------:|--------------:|
| [1]      | 2021-12-08   | 2021-12-18    |   272 |         33:41 |
| [2]      | 2021-12-18   | 2021-12-28    |   464 |         50:46 |
| [3]      | 2021-12-29   | 2022-01-19    |   643 |         61:33 |
| Total    |              |               |  1379 |        146:00 |

[1]: ./7/1 "Mass Effect 1"
[2]: ./7/2 "Mass Effect 2"
[3]: ./7/3 "Mass Effect 3"

## Summary
### Overview

This series has been a blast, its easy to see why so many folks fell in love with it.  
It gave enough freedom in choices while not to the near absurd levels of Dragon Age (requiring an external site to keep track of all choices (DA Keep))

### Themes
Each game has its own theme.

Mass Effect 1 is a Paragon game, its bright, airy, shiny and optimistic.   
You operate in Citadel space and despite some blips the universe is at peace.

Mass Effect 2 is renegade, you die in the first few minutes, you work for a terrorist organisation outside of citadel space.
Virtually all of your squadmates are scarred one way or another.   
Dialogue options give far more renegade options that paragon.  
Hundreds of humans get kidnapped and >!turned into grey goo!<.  
The game ends with a literal suicide mission.  

Mass Effect 3 is the balance of the previous games, it deals with sacrifice and compromise.  
People who were once champions got corrupted, former foes are now allies.   
Although its a renegade option it feels //justified// to stomach punch an Admiral.  
Some paragon options end up being detrimental, characters may lose themselves in grief.  
For me the final ending is Synthesis, teh compromise, the few save the many.


### Gameplay Stats
If ye have read the sub posts then you will have seen that I recorded the number of saves and hours played.
They are compiled on teh table above.

If ye tot up the saves per minute it comes out at roughly one save every six minutes, I may have some slight problems with that....
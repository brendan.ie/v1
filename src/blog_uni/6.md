+++
title   = 'CS4178 - Week 5'
date    = 2022-10-09
slug    = 6
+++

Sulis has a feature where groups can be made and assignments can be handed up as a group instead of separately.  
It also has features to creat and manage groups, even allows students to create their own groups.


Fascinating that the content that would be useful for assignment 1 was released to us (in the lecture).  
First time Swimlane is shown and explained.  
First time Use Case in roughly the same format as teh assignment is mentioned.  
At the rate that we are going through the curriculum we may not have ``Qualities and Rationale`` adequately covered before we have to do the assignment on them.


Holding back information from teh slides disadvantages everyone who is not an aural learner and is poor showing for an educator.  
By now you should have access to LENS reports, please read them.
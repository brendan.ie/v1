+++
title   = 'Git Crypto'
date    = '2021-08-25'
slug    = 3
+++

#### Nobody asked or wanted this Curse.

## How this Curse came into existence

So last night at about 3am I was part of a discussion on discord about git, at teh same time there was another //discussion// in a different channel about crypto.  
So our git discussion got tainted with stuff like:

> Person 1: ``git burn evidence``  
> ...  
> Person 2: something something git isn't a real blockchain because you can do that  
> Person 2: or conversely, blockchains are just worse git

> Person 3: blockckain based source control


As it turns out it is pretty easy to create sub commands for git so I did the //normal// thing and start thinking up of ~~cursed~~ implementations for ``git crypto``.

Just think about it, git already has hashes and local and remote copies (wallets?) so it only needs a few more tweaks to make it properly crypto!

## Cast the Curse


1. Go to:
   * Windows: ``C:\Program Files\Git\mingw64\libexec\git-core``
   * Linux: Any ``$PATH`` location such as ``/usr/bin``
2. Make a file called ``git-crypto`` there.
3. Paste in the contents below.
4. On linux you may need to make it executable.

```` bash
#!/bin/sh

# mostly taken from https://blog.sebastian-daschner.com/entries/custom-git-subcommands
# assumes a remote named origin

# get the current branch
branch=$(git symbolic-ref -q HEAD | sed -e 's|^refs/heads/||')

# got to record every transaction
git add --all
git commit -m "HODL"
# make sure you are up to date with transactions other have done
git pull --rebase origin ${branch}
# send your transactions off
git push origin ${branch}
````

Now you should be freely able to use ``git crypto`` to shove everything that you did locally into the cloud.


## Intensify the Curse

The above is a good start but it isn't fully blockchain yet, it is still mutable.

> Person 4: Ironman git, no resets to make it true Blockchain

Thankfully on *nix this can be rectified.  
To your ``~/.<shell>rc`` file add the following lines to make your wallet secure.


```` bash
# from https://stackoverflow.com/a/25886149/11964934

git() {
    if [[ "$1" = "revert" || "$1" = "reset" || "$1" =  "rebase"]]; then
        printf "No 'git %s' Only HODL.\n" "$1";
    else
      # catch sneaky amends
      if [ "$2" = "--amend" ]; then
            printf "No 'git %s' Only HODL.\n" "$1";
        else
            command git "$@";
        fi
    fi;
}
````


## Dispel the Curse

1. Delete ``git-crypto``.
2. Remove the override from ``~/.<shell>rc``.
3. Disinfect your keyboard/screen.
4. Donate to mental health services.
5. Heal your soul:
   * <<https://http.cat/>>
   * <<https://imgur.com/t/cats>>
   * <<https://www.reddit.com/r/aww/top/>>

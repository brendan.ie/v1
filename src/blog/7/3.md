+++
title       = 'Mass Effect 3'
date        = '2021-12-18'
slug        = "7/3"
skip_index  = true
+++

First thing I noticed after loading into ME3 was how smooth teh movement was compared to ME2, being able to (reasonably) easily jump over walls was so nice.  
The second thing I noticed was that the level that you achieved in ME2 got carried over, this is really neat, basically serves as saying "Hey! This is teh //same// Commander Shepherd!"

We have far fewer companions which makes a few areas of teh ship seem empty, namely where Kasumi, Mordin (no longer a science lab), Legion and Thane stayed

No real major changes gameplay wise as compared to ME2 so I am going to mostly talk about story elements.

### Base Game
This game is a bombastic flight through the galaxy, in contrast to ME1 and ME2 we have access to teh full galaxy instead of the light and dark halves.  
We have to unite a galaxy against a giant looming threat, while solving massive conflicts and minor tensions along the way.

The choices we made in ME1 and ME2 have an impact on this, some big like the Rachni Queen or small like Conrad Verner (love his story thread).

Overall the game asks some serious questions about balance of various topics.
* How to handle Geth-Quarian relations.
* The genophage and the repercussions from those choices.
* Relations between Synthetics and Organics in general.

-------------------------------------------------------------------------------------------------------------------------

On a slightly unrelated note I originally hated Donnel Udina when I started playing, but grew to grudgingly respect him.  
This was not my reaction when I played a decade ago.  
Despite being somewhat of an asshole he went above and beyond his job in trying for a better universe.  
Heck a mere 8 years after contact humanity has an embassy.  
27 years after first contact humanity has a Councilor, a feat unmatched by any other species.
In the end he was betrayed and then ended up betraying.

### Omega
This is a fun romp through Omega, a place I had fond memories of in ME2.

We dont have access to our usual teammates but rather Omega specific ones, this did upset the gameplay I was used to up to that point.

### Leviathan
This, while creepy, af filled in a heck of a lot of lore about the reapers themselves.

### From Ashes
Javik is a joy to have, his grumpy ``Get of my lawn`` attitude is a lovely contrast to teh can do attitude of Shepherd.  
I am glad that he is now included by default in teh legendary edition as he has a key role of grounding the game.  
His people failed during their cycle, but he lived to see success the next cycle.

### Citadel
This is bloody fun.  
This DLC is a look back on teh whole series, from how ammo changed to tropes like not getting shot on ladders.  
All in all it is a farewell to the series as a whole, it was a joy to see many of the characters from ME2 have softened out and have far better lives.  
It was also incredibly fun to have more than two companions on the same map.

But what got me was Mordin Solus singing, such a gut punch.

It was also interesting to play as a depowered Shepherd, one bar of health and needing to quietly take out foes,

### Ending
It may seem controversial, but I believe Synthesis to be the true ending of the series.  
ME3 in my opinion is about finding balance and compromise with a dash of sacrifice for teh greater good.

In it we see a true united galaxy, one that actually has succeeded in teh wishes of the Leviathans, life, of all sorts is now preserved.  
As opposed to what Henry Lawson though, a legacy is what you leave behind for others to make of it what they will.

Furthermore, I feel like this is the ending Bioware believes to be the true ending as well.  
It is the center path of the 4 options (forward, left, right and back).  
On top of that it is also the widest path of the three in front.

### Stats:
* Completed (100%)
    * 643 saves
    * 61:33 hours
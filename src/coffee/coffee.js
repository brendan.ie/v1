// only run on brendan.coffee

let count_total = 0;
let count_coffee = 0;
let count_tea = 0;

let coffee = 0;
let tea = 0;
function caffinate(){
  // bump these up
  count_total += 1;
  count_coffee += 1;
  count_tea += 1;

  const div = document.createElement("div");
  div.id = count_total;

  let append = false;
  let classname = "";
  let text = '';

  if(count_coffee === 3 && count_tea === 5){
    append = true;
    classname = "coffee_tea"
    text = "☕🍵";

    // reset teh counters
    count_coffee = 0;
    count_tea = 0;

    // incriment the counts
    coffee += 1;
    tea += 1;
  } else if (count_coffee === 3){
    append = true;
    classname = "coffee"
    text = "☕";

    count_coffee = 0;
    coffee += 1;
  } else if (count_tea === 5){
    append = true;
    classname = "tea"
    text = "🍵";

    count_tea = 0;
    tea += 1;
  }

  if(append){
    div.className = classname;
    div.appendChild(document.createTextNode(text));

    document.getElementById("coffee_anchor").appendChild(div);
  }

  document.getElementById("served").textContent = `${count_total} Tick-Tock. ${coffee} Coffee! ${tea} Tea!`;
}

function C0FFEE(){
  // only run it on brendan.coffee
  if(window.location.host !== "brendan.coffee"){
    return
  }

  caffinate();
  setTimeout(() => {C0FFEE()}, 1000);
}

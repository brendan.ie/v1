+++
title   = 'CS4023 - Week 9'
date    = 2022-10-31
slug    = 13
+++

[Rendered link][0]

## Structs
Structs are a good way to group data together.  
In the latest lab we have the matrix, rows and cols.  
Wouldn't it be fantastic to bundle them toether for easy access?

Structs that are used across several .c files need to go into a .h file (they are describing teh structure after all)
```C
// utils.h
struct MatrixContainer {
  int rows;
  int cols;
  float** matrix;
};
```

So now that teh struct is declared how do we assign and use it?  
In my ``utils.c`` I have this function that grabs all teh relevant data and groups it into teh struct.

```c
// utils.c
struct MatrixContainer matrixSetup() {
  // set up teh container
  struct MatrixContainer container;

  // get the rows and cols from teh first row
  scanf("%d %d", &container.rows, &container.cols);

  // assign memory for the matrix
  container.matrix = matrixMemoryAssign(container.rows, container.cols);

  return container;
}
```

You can see the return type is ``struct MatrixContainer`` and in my ``findvals.c`` it looks liek so


```c
// findvals.c
int main(int argc, char* argv[]) {
  // ...

  // function to create teh struct
  struct MatrixContainer container = matrixSetup();

  // loop through the input and assign values
  matrixFill(&container);

  // loop through teh matrix and see what ones pass
  matrixProcess(&container, &flags);

  // clean up
  matrixMemoryFree(&container);

  // ...
  return 0;
}
```

Like how I talked about [Unix philosophy][1] last week I tend to favor creating smaller functions that do one thing and do it well.  
I also like standardising teh inputs (see how tehy all take in teh struct I made).

Another interesting thing of note here is ownership.  
The var ``container`` owns the data within it (and ``main`` owns ``container``).  
We then let the other functions "borrow" it by using ``&``.  
Practically this passes a pointer to the function(s) in question which can be seen clearly on their signatures:  

Now that I look at it for the Week07 lab there was no actual need to store teh data in a matrix, can you see why?

```c
// utils.h
void matrixFill(struct MatrixContainer* container);
void matrixMemoryFree(struct MatrixContainer* container);
void matrixProcess(struct MatrixContainer* container, struct FlagsContainer* flags);
```

Let's take a look at how the memory freeing function works (since I talked about it ["last week"][1]):

```c
// utils.c

void matrixMemoryFree(struct MatrixContainer* container) {
  // free each sub array
  for (int row = 0; row < container->rows; row++) {
    free(container->matrix[row]);
  }
  // finally free the main array
  free(container->matrix);
}
```

Wait what?  
Why is there ``->`` being used to access members?  
Up in ``matrixSetup`` I could access teh matrix filed with ``container.matrix`` but here I need to use ``container->matrix``.  
The difference is that in ``matrixMemoryFree`` we dont have access to ``container`` but rather a reference to it.  
It being a reference changes things.


----------------------------------------------------------------------------------------------------------------

Interestingly enough a large part of my knowledge here comes from Rust.  
If ye have any questions about structs feel free to ask, you know how to find me.



[0]: https://brendan.ie/blog_uni/13
[1]: https://brendan.ie/blog_uni/11